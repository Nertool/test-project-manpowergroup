$(document).ready(function() {
  initSelect('.custom-select');
  logicFormControls('.form');
  togglePostInfo();
  initModal('.modal');
  logicValidForm('.form');

  var newValue = 'email';
  // gtag('event', {'form_field': 'start'});
  // gtag('event', 'form_field', 'start');

  // gtag('config', 'UA-185968410-1', {
  //   'page_title' : 'homepage',
  //   'page_path': '/home'
  // });
  $('.btn1').on('click', function (event) {
    event.preventDefault();
    gtag('set', {'field_name': 'submit_btn_1_1'});
    gtag('event', 'send_field');
  });
  $('.btn2').on('click', function (event) {
    event.preventDefault();
    gtag('set', {'field_name': 'submit_btn_2_1'});
    gtag('event', 'send_field');
  });
  $('.btn3').on('click', function (event) {
    event.preventDefault();
    gtag('set', {'field_name': 'submit_btn_3_1'});
    gtag('event', 'send_field');
  });

  $('.qwe').on('click', function (event) {
    event.preventDefault();

    // gtag('event', 'submit_field_name', {'field_name': 'first_name_2'});
    // gtag('event', 'form_field', 'click_qwe');
    // gtag('event', 'long_form', {
    //   'field_name': 'first_name'
    // });
    //
    // const gclidPromise = new Promise(resolve => {
    //   gtag('get', 'UA-185968410-1', 'form_field', resolve)
    // });
    //
    // gclidPromise.then((form_field) => {
    //   console.log('1. form_field = ', form_field)
    // });
    //
    // gtag('set', {'field_name': 'first_name_111'});
    // gtag('event', 'long_form', {
    //   'event_category': 'DC',
    //   'event_label': 'debit_card'
    // });
    //
    // gtag('event', 'long_form', {
    //   'event_category': 'DC',
    //   'event_label': 'debit_card'
    // });
    //
    // gtag('set', {'form_field': 'test222'});
    // gtag('event', 'page_view');
    // gtag('event', 'long_form', {
    //   'event_category': 'DC',
    //   'event_label': 'debit_card'
    // });
    //
    // gtag('event', 'long_form', {
    //   'event_category': 'DC',
    //   'event_label': 'debit_card',
    //   'first_name': 'first_name_2'
    // });
    //
    // gtag('get', 'UA-185968410-1', 'first_name', (first_name) => {
    //   console.log('2. first_name = ', first_name)
    // })

    gtag('set', {'field_name': 'USD_6_qwe'});
    gtag('event', 'page_view');
  })
});

function logicValidForm () {
  $('.submitForm').click(function (event) {
    // gtag('set', {'form_field': 'test333'});
    // gtag('event', 'long_form', {
    //   'event_category': 'DC',
    //   'event_label': 'debit_card'
    // });
    //
    // gtag('event', {'form_field': '5'});
    // gtag('event', 'form_field', '5');
    //
    // event.preventDefault();
    //
    // gtag('set', {'field_name': 'phone_mask'});
    //
    // gtag('event', 'long_form_2', {
    //   'field_name': 'date_birth'
    // });
    //
    // gtag('get', 'UA-185968410-1', 'field_name', (field_name) => {
    //   console.log('3. field_name = ', field_name)
    // });

    gtag('set', {'field_name': 'USD_6_submit'});

    let hasError = false;

    const parentControl = $('.required');
    let formData = {};

    parentControl.removeClass('error success');

    parentControl.each(function(_, control) {

      if ($(control).data('type') === 'email') {
        const emailInput = $(control).find('[type="email"]');
        const valueEmailInput = emailInput.val();
        const emailPattern = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;

        if (emailPattern.exec(valueEmailInput)) {
          $(control).addClass('success');
          formData.email = valueEmailInput;
        } else {
          $(control).addClass('error');
          hasError = true;

          renderTooltip(control)
        }
      } else if ($(control).data('type') === 'name') {
        const nameInput = $(control).find('[type="text"]');
        const valueNameInput = nameInput.val();
        const namePattern = /^[a-zA-Zа-яёА-ЯЁ]+$/;

        if (namePattern.test(valueNameInput)) {
          $(control).addClass('success');
          formData.name = valueNameInput;
        } else {
          $(control).addClass('error');
          hasError = true;

          renderTooltip(control)
        }
      } else if ($(control).data('type') === 'city') {
        const select = $(control).find('select');
        const selectValue = select.val();

        if (selectValue) {
          $(control).addClass('success');
          formData.city = selectValue;
        } else {
          $(control).addClass('error');
          hasError = true;

          renderTooltip(control)
        }
      } else if ($(control).data('type') === 'agree') {
        const checkbox = $(control).find('[type="checkbox"]');
        const checkboxValue = checkbox.is(':checked');

        if (checkboxValue) {
          $(control).addClass('success');
        } else {
          $(control).addClass('error');
          hasError = true;
        }
      }

    });

    if (!hasError) {
      submitForm(formData);
    }

    function renderTooltip(control) {
      const type = $(control).data('type');
      let input, message;

      if (type === 'email') {
        input = $(control).find('.form-control');
        message = 'Enter correct Email';
      } else if (type === 'name') {
        input = $(control).find('.form-control');
        message = 'Enter correct Name';
      } else if (type === 'city') {
        input = $(control).find('.select2');
        message = 'Choose city';
      }

      const cloneTooltip = $('.tooltip').clone();
      const topPos = $(input).offset().top;
      const leftPos = $(input).offset().left;
      const heightElem = $(input).height();
      const widthElem = $(input).outerWidth();

      cloneTooltip.find('.tooltip__content').text(message);
      cloneTooltip.appendTo('body');
      cloneTooltip.css({
        'top': topPos + heightElem + 10,
        'left': leftPos,
        'width': widthElem
      }).fadeIn();

      setTimeout(() => {
        cloneTooltip.fadeOut();
        cloneTooltip.remove();
      }, 3000);
    }

  });
}

function clearForm() {
  const form = $('.form');
  form.find('input').val('');
  form.find('select').val(null).trigger('change');
  form.find('[type="checkbox"]').prop('checked', false);
}

function submitForm(formData) {
  console.log(formData);
  clearForm();
}

function initModal(modal) {
  const content = $(modal).find('.description');

  $('[data-toggle="modal"]').click(function (event) {
    event.preventDefault();
    const text = $(event.target).text();

    content.text(text);
    toggleClassesVisibleModal();
  });

  $('[data-type="close"]').click(function (event) {
    event.preventDefault();
    toggleClassesVisibleModal();
    content.text('');
  });

  function toggleClassesVisibleModal() {
    $(modal).toggleClass('show');
    $('body').toggleClass('no-scroll')
  }
}

function togglePostInfo() {
  const btn = $('.btn-toggle-visible');

  btn.click(function (event) {
    event.preventDefault();
    $(this).closest('.post__text').toggleClass('open');
  });

  if ($(window).width() < 768) {
    btn.off('click')
  }
}

function logicFormControls(formSelector) {
  const form = $(formSelector)

  form.find('input').focusin(function () {
    $(this).closest('label').addClass('active');
  });

  form.find('input').focusout(function () {
    if (!$(this).val()) {
      $(this).closest('label').removeClass('active');
    }
  });

  form.find('.form-clear').click(function () {
    $(this).siblings('input').val('')
    $(this).closest('label').removeClass('active');
  })
}

function initSelect(select) {
  $(select).select2({
    placeholder: 'Select your country',
    minimumResultsForSearch: Infinity,
    selectOnClose: true
  })
}
